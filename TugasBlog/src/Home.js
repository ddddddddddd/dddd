import React, {useState} from 'react';
import {View, Button, Text, ImageBackground} from 'react-native';
import Cart from './Cart';
import Card from './Card';
import {useNavigation} from '@react-navigation/native';
import Header from './Header';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';

const Home = props => {
  const navigation = useNavigation();
  const [image, setImage] = useState();
  const TakePhotoFromCamera = () => {
    console.warn('take Photo');
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
    });
  };
  const ChooseFromGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path);
    });
  };
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 8}}>
        <Header title="Home" />
        <View style={{borderRadius: 10, width: 150, margin: 10}}>
          <Button
            onPress={() => navigation.navigate('Login')}
            title="Login mas Bro"
          />
        </View>
        <Cart total={props.order} />
        {console.log(props.order)}
        <Card
          handleBeli={() => props.addOrder()}
          handleKurang={() => props.minOrder()}
        />
        <Button title="Camera" onPress={TakePhotoFromCamera} />
        <Button title="Gallery" onPress={ChooseFromGallery} />
        <ImageBackground
          source={{uri: image}}
          style={{height: 100, width: 100}}
          imageStyle={{borderRadius: 1000}}
        />
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    order: state.carts.totalOrder,
    nama: state.user,
  };
};

const mapDispatchToProps = dispatch => ({
  addOrder: () => dispatch({type: 'ADD_ORDER'}),
  minOrder: () => dispatch({type: 'MIN_ORDER'}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
