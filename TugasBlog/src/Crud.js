import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import axios from 'axios';

const Crud = () => {
  const [title, setTitle] = useState();
  //Read get
  const getData = () => {
    axios
      .get('url API nya') //dapet data
      .then(res => console.log(res.data)); //habis dapet res.data buat data doang klo res aj ambe semua yg gaje
  };

  //post

  const postData = () => {
    let newPostData = {
      //terserah nama newPostnya. disini buat data baru
      title: 'nodeJs',
      author: 'javascript',
      //id sudah otomatis generate
    };
    axios
      .post('url API nya', newPostData) // disini masukin data barunya
      .then(res => console.log(res.data));
  };

  //update Put

  const updateData = () => {
    let newUpdateData = {
      //tuker data nya sini
      title: 'nPython',
      author: 'jGlints',
    };
    axios
      .put('url API nya/namaparent/idnya', newUpdateData) //pilih id nya disini
      .then(res => console.log('update:', res.data));
  };

  //delete

  const deleteData = () => {
    axios
      .delete('url API nya/namaparent/idnya')
      .then(res => console.log('delete:', res.data));
  };

  // katany tulis sekali aj utk ambe data
  useEffect(() => {
    getData();
  }, []);

  return (
    <View>
      <Button onPress={() => postData()} title="create" />
      <Button onPress={() => updateData()} title="update" />
      <Button onPress={() => deleteData()} title="Delete" />
    </View>
  );
};

export default Crud;

/* Create('buad') - Post,
Read (ambil data) - Get,
Update(ganti/ubah) - put (ganti semua data)/ patch(ganti data yg dipilih saja),
Delete(hapus) - delete*/

//start server : json-server --watch db.json --port 3000
//tulis di terminal portnya cincai
