import React from 'react';
import {View, Text} from 'react-native';

const Header = props => {
  return (
    <View
      style={{
        backgroundColor: 'skyblue',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{fontSize: 30, color: 'white'}}>{props.title}</Text>
    </View>
  );
};

export default Header;
