import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, Button, ScrollView} from 'react-native';
import externalStyle from '../style/externalStyle';
import axios from 'axios';
import Header from './Header';
import BlogCard from './BlogCard';
import {connect} from 'react-redux';

const Blog = props => {
  const [title, setTitle] = useState();
  const [author, setAuthor] = useState();
  const [description, setDescription] = useState();
  const [btn, setBtn] = useState('Create Blog');
  const [id, setId] = useState();
  const [dataObject, setDataObject] = useState({
    id: 1,
    title: 'yes',
    comments: [],
  });

  const getData = () => {
    axios
      .get('http://10.0.2.2:3000/Post')
      .then(res => {
        //setOutput(res.data);
        props.GetBlogSuccess(res.data);
      })
      .catch(err => console.log(err));
  };

  const postData = () => {
    let newPostData = {
      title: title,
      author: author,
      description: description,
    };
    axios
      .post('http://10.0.2.2:3000/Post', newPostData)
      .then(res => {
        console.log(res.data);
        getData();
        setTitle();
        setAuthor();
        setDescription();
      })
      .catch(err => console.log(err));
  };

  const deleteData = item => {
    axios.delete(`http://10.0.2.2:3000/Post/${item.id}`).then(res => {
      console.log('Delete:', res.data);
      getData();
    });
  };

  const updateData = () => {
    let newPostData = {
      title,
      author,
      description,
    };
    axios.put(`http://10.0.2.2:3000/Post/${id}`, newPostData).then(res => {
      console.log('update:', res.data);
      getData();
      setBtn('Create Blog');
      setTitle();
      setAuthor();
      setDescription();
    });
  };

  const changeButton = item => {
    setBtn('Update Blog');
    setTitle(item.title);
    setAuthor(item.author);
    setDescription(item.description);
    setId(item.id);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <View>
      <ScrollView>
        <Header title="Blog" />
        <View
          style={{
            width: '95%',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text
            style={{
              fontSize: 25,
              margin: 5,
              textAlign: 'center',
              fontWeight: 'bold',
            }}>
            Create Blog here
          </Text>
          <TextInput
            style={externalStyle.Input}
            onChangeText={setTitle}
            placeholder="Blog Title"
            value={title}
          />
          <TextInput
            style={externalStyle.Input}
            onChangeText={setAuthor}
            placeholder="Author"
            value={author}
          />
          <TextInput
            style={externalStyle.Input}
            onChangeText={setDescription}
            placeholder="Description"
            value={description}
          />
          <View style={externalStyle.ButtonBlog}>
            <Button
              title={btn}
              onPress={() => (btn == 'Create Blog' ? postData() : updateData())}
            />
          </View>
        </View>
        <View>
          <Text style={{fontSize: 25, fontWeight: 'bold'}}>Blogs:</Text>
          <View>
            {props.reduxBlogs.length > 0 &&
              props.reduxBlogs.map((item, index) => (
                <BlogCard
                  key={index}
                  title={item.title}
                  author={item.author}
                  description={item.description}
                  deleteData={() => deleteData(item)}
                  updateData={() => changeButton(item)}
                />
              ))}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = state => ({
  reduxBlogs: state.blogs.dataBlogs,
});

const mapDispatchToProps = dispatch => ({
  getBlogs: data => dispatch({type: 'GET_BLOGS'}),
  createBlog: data => dispatch({type: 'CREATE_BLOGS'}),
  GetBlogSuccess: data => dispatch({type: 'GET_BLOGS_SUCCESS', data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Blog);
