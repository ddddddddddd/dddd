import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {Text, View, TextInput, Button} from 'react-native';
import {connect} from 'react-redux';
import externalStyle from '../style/externalStyle';

const Login = props => {
  const navigation = useNavigation();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [selectedLanguage, setSelectedLanguage] = useState();
  const handleLogin = () => {
    const newData = {
      username,
      password,
    };
    props.login(newData);
  };

  return (
    <View style={externalStyle.container}>
      <View style={externalStyle.header}>
        <Text style={externalStyle.loginStyle}>Login</Text>
      </View>
      <View style={externalStyle.body}>
        <Text style={externalStyle.textStyle}>Username</Text>
        <TextInput
          style={externalStyle.input}
          placeholder="Enter Your Username"
          onChangeText={text => setUsername(text)}
          value={username}
        />
        <Text style={externalStyle.textStyle}>Password</Text>
        <TextInput
          style={externalStyle.input}
          placeholder="Enter Your Password"
          underlineColorAndroid="transparent"
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
          value={password}
        />
        <View style={externalStyle.buttonContainer}>
          <Button title="Sign In" onPress={() => handleLogin()} />
        </View>
        <Text style={externalStyle.Forget}>Forget Password?</Text>
        <Text
          style={externalStyle.signUp}
          onPress={() => navigation.navigate('Register')}>
          Didn't have account, sign up here!
        </Text>
      </View>
    </View>
  );
};

const reduxState = state => ({
  verify: state.auth.isLoggedIn,
});

const reduxDispatch = dispatch => ({
  login: dataLogin => dispatch({type: 'LOGIN', data: dataLogin}),
});

export default connect(reduxState, reduxDispatch)(Login);
