import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const ProfileCard = props => {
  return (
    <View
      style={{
        borderWidth: 1,
        padding: 15,
        backgroundColor: 'wheat',
        width: '60%',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 15,
        borderRadius: 8,
        elevation: 8,
      }}>
      <Image
        style={{
          height: 100,
          width: 100,
          marginTop: 10,
          borderWidth: 1,
          borderRadius: 10,
        }}
        source={require('./image.jpg')}
      />
      <Text>{props.nama}</Text>
      <Text>{props.email}</Text>
    </View>
  );
};

export default ProfileCard;
