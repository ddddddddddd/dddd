import React from 'react';
import {View, Text} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Cart = ({total}) => {
  //total = 0;

  return (
    <View
      style={{
        height: 100,
        width: 100,
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 16,
      }}>
      <AntDesign name="shoppingcart" size={50} color="blue" />
      <Text
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
          backgroundColor: 'gray',
          borderRadius: 30,
          fontSize: 20,
          width: 30,
          height: 30,
          textAlign: 'center',
          color: 'white',
        }}>
        {total}
      </Text>
    </View>
  );
};

export default Cart;
