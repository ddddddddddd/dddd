import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Home from './Home';
import Blog from './Blog';
import Profile from './Profile';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Login from './Login';

const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
};

const BlogScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Blog" component={Blog} />
    </Stack.Navigator>
  );
};

const ProfileScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
};

const BottomTabs = () => {
  return (
    <BottomTab.Navigator initialRouteName={'Home'}>
      <BottomTab.Screen
        name="Home"
        headerMode="none"
        component={HomeScreen}
        options={{
          tabBarVisible: true,
          tabBarIcon: ({focused}) => (
            <AntDesign name="home" size={25} color="blue" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Blog"
        component={BlogScreen}
        options={{
          tabBarVisible: true,
          tabBarIcon: ({focused}) => (
            <AntDesign name="form" size={25} color="blue" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarVisible: true,
          tabBarIcon: ({focused}) => (
            <AntDesign name="smileo" size={25} color="blue" />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default BottomTabs;
