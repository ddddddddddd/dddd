const initialState = {
  totalOrder: 0,
  user: 'dan',
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ORDER':
      return {
        ...state,
        totalOrder: state.totalOrder + 1,
      };
    case 'MIN_ORDER':
      if (state.totalOrder == 0) {
        return state;
      }
      return {
        ...state,
        totalOrder: state.totalOrder - 1,
      };
    default:
      return state;
  }
};

export default rootReducer;
