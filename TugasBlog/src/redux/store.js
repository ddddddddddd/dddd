import {createStore, applyMiddleware} from 'redux';
import rootReducer from './index';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga/index';

const sagaMiddleWare = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleWare));

sagaMiddleWare.run(rootSaga);

export default store;
