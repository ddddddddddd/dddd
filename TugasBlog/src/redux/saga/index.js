import {all} from 'redux-saga/effects';
import blogs from './blogs';
import authSaga from './authSaga';

export default function* rootSaga() {
  yield all([blogs(), authSaga()]);
}
