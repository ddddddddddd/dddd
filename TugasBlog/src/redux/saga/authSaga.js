import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {saveToken, removeToken} from '../../Comp/function';

function* Login(action) {
  try {
    console.log('login running');
    const resLogin = yield axios({
      method: 'POST',
      url: 'https://blogger-glints.herokuapp.com/user/login',
      data: action.data,
    });
    console.log(resLogin.data);
    yield saveToken(resLogin.data.token);
    yield put({type: 'LOGIN_SUCCESS'});
    console.log('login sukses');
  } catch (err) {
    console.log(err);
    console.log('login gagal');
    yield put({type: 'LOGIN_ERROR'});
  }
}

function* logout() {
  try {
    yield removeToken();
    console.log('telah logout');
  } catch (err) {
    console.log(err);
  }
}
export default function* authSaga() {
  yield takeLatest('LOGIN', Login);
  yield takeLatest('LOGOUT', logout);
}
