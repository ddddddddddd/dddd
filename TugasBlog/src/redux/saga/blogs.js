import axios from 'axios';
import {takeLatest, put} from '@redux-saga/core/effects';
import {baseUrl} from '../../Comp/API';
import {getHeaders} from '../../Comp/function';

function* getBlogs(action) {
  try {
    const resBlogs = yield axios.get('http://10.0.2.2:3000/Post');
    yield put({type: 'GET_BLOGS_SUCCESS', data: resBlogs.data});
  } catch (err) {
    console.log(err);
  }
}

function* blogsSaga() {
  yield takeLatest('GET_BLOGS', getBlogs);
  yield takeLatest('CREATE_BLOGS', createBlog);
}

function* createBlog(action) {
  try {
    console.log('Creating blog');
    const headers = yield getHeaders();

    const resCreateBlog = yield axios({
      method: 'POST',
      url: `${baseUrl}/blog/create`,
      data: action.data,
      headers,
    });

    if (resCreateBlog && resCreateBlog.data) {
      console.log(resCreateBlog.data);
      yield put({type: 'CREATE_BLOG_SUCCESS'});
      yield put({type: 'GET_BLOGS'});
    }
    console.log('Create blog success');
  } catch (err) {
    console.log(err);
    yield put({type: 'CREATE_BLOG_FAILED'});
  }
}

export default blogsSaga;
