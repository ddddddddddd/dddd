import {combineReducers} from 'redux';
import carts from './carts';
import profile from './profile';
import blogs from './blogs';
import auth from './auth';

export default combineReducers({
  carts,
  profile,
  blogs,
  auth,
});
