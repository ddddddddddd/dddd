const initialState = {
  dataBlogs: [],
  isLoading: false,
};

const blogs = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_BLOGS':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_BLOGS_SUCCESS':
      return {
        ...state,
        dataBlogs: action.data,
        isLoading: false,
      };
    case 'CREATE BLOGS':
      return {
        ...state,
        isLoading: true,
      };
    case 'CREATE_BLOGS_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'CREATE_BLOGS_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default blogs;
