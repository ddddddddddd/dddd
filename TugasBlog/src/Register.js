import React, {useState} from 'react';
import {Text, View, TextInput, Button, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import externalStyle from '../style/externalStyle';

const Register = props => {
  const [FullName, setFullName] = useState();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [age, setAge] = useState();

  return (
    <View style={externalStyle.container}>
      <View style={externalStyle.header}>
        <Text style={externalStyle.RegStyle}>Register</Text>
      </View>
      <View style={externalStyle.bodyReg}>
        <Text style={externalStyle.textStyleReg}>Username</Text>
        <TextInput
          style={externalStyle.inputReg}
          placeholder="Enter Your Username"
          onChangeText={text => setUsername(text)}
          value={username}
        />
        <Text style={externalStyle.textStyleReg}>Full Name</Text>
        <TextInput
          style={externalStyle.inputReg}
          placeholder="Enter Your Full Name"
          onChangeText={text => setFullName(text)}
          value={FullName}
        />
        <Text style={externalStyle.textStyleReg}>Age</Text>
        <TextInput
          style={externalStyle.inputReg}
          keyboardType="numeric"
          placeholder="Enter Your Age"
          onChangeText={text => setAge(text)}
          value={age}
        />
        <Text style={externalStyle.textStyleReg}>Password</Text>
        <TextInput
          style={externalStyle.inputReg}
          placeholder="Enter Your Password"
          underlineColorAndroid="transparent"
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
          value={password}
        />
        <View style={externalStyle.buttonSignUp}>
          <Button title="Sign Up" />
        </View>
        <View style={{flexDirection: 'row', marginTop: 25}}>
          <Text style={{fontWeight: 'bold'}}>Already Have an Account ? </Text>
          <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
            <Text
              style={{
                textDecorationLine: 'underline',
                color: 'blue',
                fontWeight: 'bold',
              }}>
              Sign In
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Register;
