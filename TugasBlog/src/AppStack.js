import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import RootStack from './RootStack';
import Login from './Login';
import Register from './Register';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();

const AppStack = props => {
  const getToken = async () => {
    const value = await AsyncStorage.getItem('token');
    if (value !== null) {
      props.checkToken();
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  return (
    <Stack.Navigator headerMode="none">
      {props.verify ? (
        <Stack.Screen name="Main" component={RootStack} />
      ) : (
        <>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
        </>
      )}
    </Stack.Navigator>
  );
};

const reduxState = state => ({
  verify: state.auth.isLoggedIn,
});

const reduxDispatch = dispatch => ({
  checkToken: () => dispatch({type: 'VERIFY_TOKEN'}),
});

export default connect(reduxState, reduxDispatch)(AppStack);
