import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import externalStyle from '../style/externalStyle';

const Card = props => {
  return (
    <View
      style={{justifyContent: 'center', alignItems: 'center', marginTop: 30}}>
      <View style={externalStyle.CardContainer}>
        <Image
          style={{
            height: 100,
            width: 100,
            marginTop: 10,
            borderWidth: 1,
            borderRadius: 10,
          }}
          source={require('./image.jpg')}
        />
        <Text style={{margin: 10}}>Gambar Rumah</Text>
        <Text style={{marginBottom: 10, color: 'blue'}}> Rp. 20.000.000</Text>
        <View
          style={{
            flexDirection: 'row',
            width: '95%',
            borderRadius: 10,
            overflow: 'hidden',
          }}>
          <TouchableOpacity
            style={{
              width: '15%',
              backgroundColor: 'blue',
              justifyContent: 'center',
            }}
            onPress={() => props.handleKurang()}>
            <Text style={{fontSize: 15, textAlign: 'center', color: 'white'}}>
              -
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              padding: 10,
              backgroundColor: 'white',
              borderColor: 'grey',
            }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'black',
              }}>
              Beli
            </Text>
          </View>
          <TouchableOpacity
            style={{
              width: '15%',
              backgroundColor: 'blue',
              justifyContent: 'center',
            }}
            onPress={() => props.handleBeli()}>
            <Text style={{fontSize: 15, textAlign: 'center', color: 'white'}}>
              +
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Card;
