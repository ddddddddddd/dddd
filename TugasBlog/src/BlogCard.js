import React from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';

const BlogCard = props => {
  return (
    <TouchableWithoutFeedback onPress={() => props.updateData()}>
      <View style={{borderWidth: 1, margin: 10, borderRadius: 4}}>
        <Text style={{fontSize: 25, fontWeight: 'bold', paddingLeft: 10}}>
          {props.title}
        </Text>
        <Text style={{fontSize: 18, paddingLeft: 10}}>{props.author}</Text>
        <Text style={{paddingLeft: 10}}>{props.description}</Text>
        <TouchableWithoutFeedback onPress={() => props.deleteData()}>
          <Text style={{position: 'absolute', right: 10, top: 10}}>X</Text>
        </TouchableWithoutFeedback>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default BlogCard;
