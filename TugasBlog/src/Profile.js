import React from 'react';
import {View, Text, Button} from 'react-native';
import ProfileCard from './ProfileCard';
import Header from './Header';
import {connect} from 'react-redux';

const Profile = props => {
  return (
    <View style={{flex: 9}}>
      <Header title="Profile" />
      <ProfileCard nama={props.nama} email={props.email} />
      <View style={{justifyContent: 'flex-end', flex: 3}}>
        <Button title="Logout" onPress={() => props.logout()} />
      </View>
    </View>
  );
};

const mapStateToProps = state => ({
  nama: state.profile.nama,
  email: state.profile.email,
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch({type: 'LOGOUT'}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
