import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import BottomTabs from './BottomTabs';
import Login from './Login';
import Register from './Register';
import Home from './Home';

const RootStack = createStackNavigator();
const RootStackScreen = () => {
  return (
    <RootStack.Navigator headerMode="none" initialRouteName="BottomTabs">
      <RootStack.Screen name="BottomTabs" component={BottomTabs} />
      <RootStack.Screen name="Home" component={Home} />
    </RootStack.Navigator>
  );
};
export default RootStackScreen;
