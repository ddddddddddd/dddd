import {StyleSheet} from 'react-native';

const externalStyle = StyleSheet.create({
  CardContainer: {
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'grey',
    width: 150,
  },
  Input: {
    borderWidth: 1,
    borderRadius: 10,
    margin: 5,
  },
  ButtonBlog: {
    borderWidth: 1,
    borderRadius: 10,
    margin: 5,
  },
  TheBlog: {
    borderWidth: 1,
    margin: 10,
    borderRadius: 4,
  },
  loginStyle: {
    color: 'blue',
    fontSize: 40,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    width: 200,
  },
  body: {
    backgroundColor: 'silver',
    padding: 20,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
  },
  buttonContainer: {
    marginTop: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: 'black',
    padding: 8,
    margin: 10,
    width: 200,
    borderRadius: 6,
  },
  textStyle: {
    textAlign: 'center',
  },
  bodyReg: {
    backgroundColor: 'silver',
    padding: 20,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'flex-start',
  },
  inputReg: {
    borderWidth: 1,
    borderColor: 'black',
    padding: 8,
    margin: 10,
    width: 250,
    borderRadius: 6,
    alignItems: 'flex-start',
  },
  textStyleReg: {
    fontSize: 14,
    paddingLeft: 12,
  },
  buttonSignUp: {
    marginTop: 20,
    paddingLeft: 12,
    borderRadius: 4,
  },
  RegStyle: {
    color: 'blue',
    fontSize: 40,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  signUp: {
    marginTop: 20,
    color: 'blue',
    textDecorationLine: 'underline',
  },
  Forget: {
    textAlign: 'center',
    marginTop: 10,
    color: 'blue',
  },
  flint: {},
});

export default externalStyle;
